Technical test Nicolas Diaz
this documents explain how was my approach on the streamlines technical assesment

Data management:
i decided to use scriptable objects for storing the game data i structure them as, Wearable, Collectables and general data.
to play around with the values you need to navigate to Assets/Data/ there you will find two scriptable objects one for jetpacks and another for the sphere.
the jetpacks system works based on ids you can create under jetpacksData Scriptable object as many different data packs for jetpacks just assigned a unique id to it and match it with a clone of a jetpack on scene or modify one of the added.


under the jetpacks scriptable object you can modify quite a bit parameters of jetpacks including their key to interacts with the jetpack (you can add different keys for every jetpack if wanted) by default you will be able to play with the left shift


Managers:
There are different managers for controlling the game, i implement here the well known singleton pattern for making sure there is just one single instance of every manager in order to let me call them easily, this singletons works as an instance of <T> implementing them as parent classes letting me instantiate them on the scene and store information.

 i will list here some of them and their general purpose:
InputManager, this one handles all the user inputs and communicates with other managers to send instructions 
DataManager, this has the role of loading the information from the scriptable objects and set them to the objects in the scene, also it serialize an deserialize information for the leaderboard.
UImanager, handles all the ui libraries and basically its purpose is to display information.
ResultsManager, this one stores all the collected points and calculates the final results to show on the leaderboard

Inventory system:


the inventory system let the user interacts and use wearables, the user can equip and unequip wearables of different types, just collide with the objects on the scene and it will grab those items, some items just admit one their types on the list (as jetpacks) so when the user collide with another wearable it will just drop the current an equip the other one, if in the future we will want to create another wearable ius just as easy as create another class that inherits from Wearable

the wearable class has an enumerator for assigning the type of wearable

there is also another parent class called Collectable that handle how the user grabs items and some virtual methods to override certain actions.

General Architecture:
i create multiple interfaces and parent classes for avoiding duplicated code, the architecture was thinked to add more elements in the future easily and to control their behaviour individually, if you want to add anothe entity that can move or that can collect or is as easy as to inherit from the parent class or to implement the interface, also there are some abstract and virtual classes to override action methods.
image.png



