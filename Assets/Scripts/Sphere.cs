using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sphere : JumperRoller
{
    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    public void Update()
    {
        rb.useGravity = true;
    }
}
