using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class SerializableJetpacksData
{
    [SerializeField]
    public string jetPackName;
    [SerializeField]
    public KeyCode keyToInteract = KeyCode.LeftShift;
    [SerializeField]
    public int id;
    [SerializeField]
    public float maxfuel = 170f;
    [SerializeField]
    public float fuelwasteStep = 10;
    [SerializeField]
    public float maxHeight = 20f;
    [SerializeField]
    public float horsePower = 0f;
    [SerializeField]
    public float rechargingSpeed = 5f;

}
