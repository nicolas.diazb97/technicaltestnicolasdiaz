using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

public class DataManager : Singleton<DataManager>
{
    public SphereData SphereData;
    public JetPacksData JetPackData;
    public Sphere sphere;
    public List<JetPack> jetPacks;
    public PlayerData cuurScoresData =new PlayerData();
    private void Start()
    {
        int newuser = PlayerPrefs.GetInt("newuser2", 0);
        if (newuser == 0)
            Save(0);
        sphere.SetData(SphereData.verticalSpeed, SphereData.horizontalSpeed);
        SetJetPackData();
        Load();
        PlayerPrefs.SetInt("newuser2", 1);
    }
    public List<int> GetScores()
    {
        return cuurScoresData.scores;
    }
    public void Save(int _newScore)
    {
        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.Create(Application.persistentDataPath + "/score.dat");
        cuurScoresData.scores.Add(_newScore);
        bf.Serialize(file, cuurScoresData);
        file.Close();
    }
    public void Load()
    {
        if (File.Exists(Application.persistentDataPath + "/score.dat"))
        {
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.Open(Application.persistentDataPath + "/score.dat", FileMode.Open);
            cuurScoresData = (PlayerData)bf.Deserialize(file);
            file.Close(); /// you need to add here all what you need
        }
    }
    void SetJetPackData()
    {
        foreach (var item in jetPacks)
        {
            List<SerializableJetpacksData> matchedData = JetPackData.jetpacks.Where(j => j.id == item.id).ToList();
            if (matchedData.Count > 0)
            {
                item.SetData(matchedData[0].maxfuel, matchedData[0].fuelwasteStep, matchedData[0].maxHeight, matchedData[0].horsePower, matchedData[0].rechargingSpeed,matchedData[0].keyToInteract);
            }
        }
    }
}
public static class ExtensionMethods
{

    public static float Remap(this float value, float from1, float to1, float from2, float to2)
    {
        return (value - from1) / (to1 - from1) * (to2 - from2) + from2;
    }

}
