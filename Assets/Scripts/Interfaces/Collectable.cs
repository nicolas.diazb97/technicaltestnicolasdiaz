using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Collectable : MonoBehaviour
{
    public int points;
    bool collected;
    public virtual void Grab()
    {
        gameObject.SetActive(false);
        collected = true;
        ResultsManager.main.addPoints(points);
    }
    private void OnTriggerEnter(Collider other)
    {
        if (!collected)
        {
            if (other.GetComponent<Inventory>())
            {
                Grab();
                other.GetComponent<Inventory>().Collect(this);
            }
        }
    }
}
