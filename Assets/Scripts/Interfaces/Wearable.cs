using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Wearable :MonoBehaviour
{
    public int id;
    [HideInInspector]
    public KeyCode keyToInteract;
    public UnityEvent uiEventsOnInit;
    public enum WearableType {Jetpack};
    public WearableType wearableType;


    public virtual void Init()
    {
        uiEventsOnInit.Invoke();
    }

    public virtual void Equip()
    {
    }
    public virtual void UnEquip()
    {
    }
    public virtual void Interact(Sphere sphere)
    {

    }

}
