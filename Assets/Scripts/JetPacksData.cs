using System;
using UnityEngine;

[CreateAssetMenu(fileName = "jetpackData", menuName = "jetpackData", order = 1)]
public class JetPacksData : ScriptableObject
{
    public SerializableJetpacksData[] jetpacks;
}