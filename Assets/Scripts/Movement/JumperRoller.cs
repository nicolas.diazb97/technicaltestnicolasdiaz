﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JumperRoller : MonoBehaviour, IMovement
{
    float speed = 10f;
    float jumpSpeed = 7f;
    [HideInInspector]
    public Rigidbody rb;
    float distToGround = 0.5f;
    // Start is called before the first frame update
    public void Move(float x, float z)
    {
        Vector3 movement = new Vector3(x, 0, z);
        movement = Vector3.ClampMagnitude(movement, 1);
        transform.Translate(movement * speed * Time.deltaTime);
        //rb.velocity = new Vector3(x, rb.velocity.y, z) * speed;
    }

    public void Jump()
    {
        if (IsGrounded())
        {
            rb.AddForce(Vector3.up * jumpSpeed, ForceMode.Impulse);
        }
    }
    public void SetData(float verticalSpeed, float horizontalSpeed)
    {
        speed = horizontalSpeed;
        jumpSpeed = verticalSpeed;
    }
    public bool IsGrounded()
    {
        Vector3 dir = new Vector3(0, -1, 0);
        RaycastHit hit;
        Debug.DrawRay(transform.position, dir * distToGround, Color.green);
        if (Physics.Raycast(transform.position, dir, out hit, distToGround))
        {
            return true;
        }
        else
        {
            return false;
        }
    }
}
