using System;
using UnityEngine;

[CreateAssetMenu(fileName = "sphereData", menuName = "sphereData", order = 1)]
public class SphereData : ScriptableObject
{
    public float horizontalSpeed;
    public float verticalSpeed;
}