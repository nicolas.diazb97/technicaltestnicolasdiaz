using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JetPack : Wearable
{
    public bool equipped;
    float currfuel;
    float maxfuel = 170f;
    float fuelwasteStep = 10;
    float maxHeight = 20f;
    float velocity = 0f;
    float initialVelocity;
    float rechargingSpeed = 1f;
    Sphere sphere;
    bool onCoolDown;

    public void SetData(float _maxFuel, float _fuelwasteStep, float _maxHeight, float _Hps, float _rechargingSpeed, KeyCode _keycode)
    {
        Debug.Log("data setted");
        wearableType = Wearable.WearableType.Jetpack;
        maxfuel = _maxFuel;
        fuelwasteStep = _fuelwasteStep;
        maxHeight = _maxHeight;
        initialVelocity = _Hps;
        velocity = _Hps;
        rechargingSpeed = _rechargingSpeed;
        currfuel = maxfuel;
        keyToInteract = _keycode;
    }
    public override void Interact(Sphere _sphere)
    {
        sphere = _sphere;
        base.Interact(_sphere);
        if (currfuel >= 0)
        {
            _sphere.rb.useGravity = false;
            Debug.LogError("holding");
            velocity += Time.deltaTime;
            _sphere.transform.Translate(new Vector3(0, velocity, 0) * Time.deltaTime);
            //sphere.transform.position = new Vector3(sphere.transform.position.x, sphere.transform.position.y * Time.deltaTime, sphere.transform.position.z);
            currfuel -= Time.deltaTime * fuelwasteStep;
            if (_sphere.transform.position.y >= maxHeight)
            {
                _sphere.transform.position = new Vector3(_sphere.transform.position.x, maxHeight, _sphere.transform.position.z);
            }
        }
        else
        {
            _sphere.rb.useGravity = true;
        }
    }
    public override void UnEquip()
    {
        base.UnEquip();
        sphere = null;
        onCoolDown = true;
        StartCoroutine(WaitCoolDown());
        equipped = false;
    }
    public void Update()
    {
        if (sphere)
        {
            if (sphere.IsGrounded())
            {
                if (currfuel < maxfuel)
                {
                    currfuel += rechargingSpeed * Time.deltaTime;
                }
                velocity = initialVelocity;
            }
            UiManager.main.SetFuel(currfuel.Remap(0, maxfuel, 0.00f, 1.00f));
        }
    }
    IEnumerator WaitCoolDown()
    {
        yield return new WaitForSecondsRealtime(2f);
        onCoolDown = false;
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.GetComponent<Inventory>() && !equipped)
        {
            if (!onCoolDown)
            {
                other.GetComponent<Inventory>().Equip(this);
                equipped = true;
            }
        }
    }
}
