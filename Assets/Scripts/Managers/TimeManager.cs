using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class TimeManager : MonoBehaviour
{
    public float timeRemaining = 10;
    public bool timerIsRunning = false;
    private void Start()
    {
        // Starts the timer automatically
        timerIsRunning = true;
    }
    void Update()
    {
        if (timerIsRunning)
        {
            if (timeRemaining > 0)
            {
                timeRemaining -= Time.deltaTime;
                UiManager.main.DisplayTime(timeRemaining);
            }
            else
            {
                if (timerIsRunning)
                {
                    ResultsManager.main.CalculateLeaderBoard();
                    timeRemaining = 0;
                    timerIsRunning = false;
                }
            }
        }
    }

}