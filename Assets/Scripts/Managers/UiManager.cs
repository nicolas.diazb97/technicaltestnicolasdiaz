using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class UiManager : Singleton<UiManager>
{
    public Slider fuel;
    public TextMeshProUGUI timerText;
    public GameObject positionPrefab;
    public LeaderBoard leaderBoard;
    public GameObject leaderBoardHolder;
    bool ownScored;
    public void SetFuel(float value)
    {
        fuel.value = value;
    }
    public void ShowFinalResults(List<int> organizedScores, int ownPoints)
    {
        leaderBoardHolder.SetActive(true);
        DataManager.main.sphere.enabled = false;
        foreach (var item in organizedScores)
        {
            GameObject tempPos = Instantiate(positionPrefab, leaderBoard.transform);
            tempPos.GetComponentInChildren<TextMeshProUGUI>().text = item.ToString();
            if(item == ownPoints&&!ownScored)
            {
                ownScored = true;
                tempPos.GetComponentInChildren<TextMeshProUGUI>().text += " (you)";
            }
        }
    }
    public void DisplayTime(float timeToDisplay)
    {
        timeToDisplay += 1;
        float minutes = Mathf.FloorToInt(timeToDisplay / 60);
        float seconds = Mathf.FloorToInt(timeToDisplay % 60);
        timerText.text = string.Format("{0:00}:{1:00}", minutes, seconds);
    }
}
