using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputManager : Singleton<InputManager>
{
    public Sphere sphere;
    public Inventory inventory;


    // Update is called once per frame
    void Update()
    {
        sphere.Move(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical"));
        CheckJump();
    }
    private void LateUpdate()
    {
        CheckInteractionsOnWearables();
    }
    void CheckInteractionsOnWearables()
    {
        foreach (var wearable in inventory.currWearables)
        {
            if (Input.GetKey(wearable.keyToInteract))
            {
                wearable.Interact(sphere);
            }
        }
        
    }
    void CheckJump()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            sphere.Jump();
        }
    }
}
