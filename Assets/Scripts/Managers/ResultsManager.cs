using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class ResultsManager : Singleton<ResultsManager>
{
    int currPoints;
    public void addPoints(int _points)
    {
        currPoints+=_points;
    }
    public void CalculateLeaderBoard()
    {
        DataManager.main.Save(currPoints);
        List<int> savedScores = DataManager.main.GetScores();
        savedScores = savedScores.OrderByDescending(w => w).ToList();
        UiManager.main.ShowFinalResults(savedScores, currPoints);
    }
}
