using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Inventory : MonoBehaviour
{
    public List<Wearable> currWearables;
    public List<Collectable> currCollectables;
    // Start is called before the first frame update
    public void Equip(Wearable wearable)
    {
        List<Wearable> matchedData = currWearables.Where(w => w.wearableType == Wearable.WearableType.Jetpack).ToList();
        if (matchedData.Count > 0)
        {
            UnEquip(matchedData[0]);
        }
        currWearables.Clear();
        currWearables.Add(wearable);
        wearable.Init();
        wearable.transform.position = GetComponentInChildren<ContactPoint>().transform.position;
        wearable.transform.SetParent(GetComponentInChildren<ContactPoint>().transform);
    }
    public void UnEquip(Wearable wearable)
    {
        currWearables.Remove(wearable);
        wearable.UnEquip();
        wearable.transform.SetParent(null);
    }
    public void Collect(Collectable collectable)
    {
        currCollectables.Add(collectable);
    }
}
